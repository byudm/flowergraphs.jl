# Changelog

## [0.3.1]
### Added
- Changelog

### Deprecated
- Both `SunflowerGraph` and `BoxflowerGraph`
  - Use `sunflower_graph` instead of `SunflowerGraph`
  - Use `boxflower_graph` instead of `BoxflowerGraph`
  This is in accordance with the julia naming convention

### Improved
- Performance of `sunflower_graph` and `boxflower_graph` is several times better due to vectorized operations
