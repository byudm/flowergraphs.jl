module FlowerGraphs

using LightGraphs

include("SunflowerGraphs/SunflowerGraphs.jl")

using .SunflowerGraphs

export sunflower_graph, boxflower_graph, SunflowerGraph, BoxflowerGraph

end
