module SunflowerGraphs

import LightGraphs:
	nv, add_edge!, add_vertex!, add_vertices!, SimpleGraph, cycle_graph

export sunflower_graph,
	   boxflower_graph

"""
	sunflower_graph(n)

Creates a Sunflower graph with `n` petals. Throws a `DomainError` if n < 3.

# Examples
```julia-repl
julia> sunflower_graph(3)
{6, 9} undirected simple Int64 graph
```
"""
function sunflower_graph(n::T) where T <: Integer
	n < 3 && throw(DomainError("sunflower must contain at least 3 components"))

	ne = 3n
	fadjlist = Vector{Vector{T}}(undef, 2n)
	@inbounds fadjlist[1]   = T[2, n, 2n, n+1]
	@inbounds fadjlist[n]   = T[1, n-1, 2n, 2n-1]
	@inbounds fadjlist[n+1] = T[1, 2]
	@inbounds fadjlist[2n]  = T[1, n]

	@inbounds @simd for u = 2:(n-1)
		fadjlist[u]   = T[u-1, u+1, n+u-1, n+u]
		fadjlist[n+u] = T[u, u+1]
	end

	SimpleGraph(ne, fadjlist)
end

@deprecate SunflowerGraph sunflower_graph

function boxflower_graph(n::T) where T <: Integer
	n < 3 && throw(DomainError("sunflower must contain at least 3 components"))

	ne = 4n
	fadjlist = Vector{Vector{T}}(undef, 3n)
	@inbounds fadjlist[1]    = T[2, n, n+1, 3n]
	@inbounds fadjlist[n]    = T[1, n-1, 3n-2, 3n-1]
	@inbounds fadjlist[n+1]  = T[1, n+2]
	@inbounds fadjlist[3n]   = T[1, 3n-1]
	@inbounds fadjlist[3n-1] = T[n, 3n]
	@inbounds fadjlist[3n-2] = T[n, 3*(n-1)]

	@inbounds @simd for u = 2:(n-1)
		outer = n + 2*(u-1)

		fadjlist[u]       = T[u-1, u+1, outer, outer+1]
		fadjlist[outer]   = T[u, outer-1]
		fadjlist[outer+1] = T[u+1, outer+2]
	end

	SimpleGraph(ne, fadjlist)
end

@deprecate BoxflowerGraph boxflower_graph

end
